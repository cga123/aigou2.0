package com.hbgc.mapper;

import com.hbgc.entity.Product;
import com.hbgc.entity.ReceivingAddress;
import com.hbgc.entity.ShopCar;
import com.hbgc.entity.ShopCartProduct;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface FrontShopCarMapper {

    @Select("SELECT * FROM s_receiving_address WHERE user_id = #{userid}")
    List<ReceivingAddress> findAddsByUserId(String userid);

    @Select("SELECT id FROM s_shop_cart WHERE user_id = #{userid}")
    String findCarIdByUserId(String userid);


    @Select("SELECT * FROM s_shop_cart_product WHERE shop_cart_id = #{carId}")
    List<ShopCartProduct> findCartProductByCarId(String carId);

    @Select("SELECT * FROM s_product WHERE id =#{productId}")
    Product findProductByPId(String productId);

    @Delete("DELETE FROM s_shop_cart_product WHERE id = #{id} ")
    int deleteProductById(String id);


    int deleteProductAllById(@Param("ids") List<String> ids);



    //通过用户ID查询购物车
    @Select("SELECT id, cart_id, user_id FROM s_shop_cart WHERE user_id =#{userId}")
    ShopCar getShopCartByUserId(@Param("userId") String userId);
   //添加购物车
    @Select("INSERT INTO s_shop_cart (id, cart_id, user_id) VALUES(#{id},#{cartId},#{userId})")
   void addShopCart(ShopCar ShopCart);
}
