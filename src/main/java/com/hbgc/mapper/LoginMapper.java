package com.hbgc.mapper;

import com.hbgc.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;


public interface LoginMapper {

    @Select("select * from s_user where username=#{username} and type = #{type}")
    User findByUsername(@Param("username") String username, @Param("type") int type);
}
