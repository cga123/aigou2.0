package com.hbgc.mapper;

import com.hbgc.entity.Brand;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


public interface BrandMapper {

    @Select("SELECT sd.id,brand_name as brandName,product_type_name as brandType,brand_img as brandImg\n" +
            "FROM s_brand sd,s_product_type sp\n" +
            "WHERE sd.brand_type=sp.id limit #{start},#{size}")
    List<Brand> findByAll(Map<String, Object> map);

    @Delete("delete from s_brand where id=#{id} ")
    int del(String id);

    @Select("SELECT sd.id,brand_name as brandName,product_type_name as brandType,brand_img as brandImg\n" +
            "FROM s_brand sd,s_product_type sp\n" +
            "WHERE sd.brand_type=sp.id and sd.id=#{id}")
    Brand findById(String id);


    @Update("UPDATE s_brand  SET brand_name = #{brandName},brand_type = #{brandType},brand_img = #{brandImg}  where id = #{id}")
    int update(Brand brand);

    @Insert("insert into s_brand (id,brand_name,brand_type,brand_img) value(#{id},#{brandName},#{brandType},#{brandImg}) ")
    int add(Brand brand);



    @Select("SELECT sd.id,brand_name as brandName,product_type_name as brandType,brand_img as brandImg\n" +
            "FROM s_brand sd,s_product_type sp")
    List<Brand> findByAllA();
}
