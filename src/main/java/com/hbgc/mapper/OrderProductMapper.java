package com.hbgc.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * 订单销量持久层接口
 */
public interface OrderProductMapper {
    //通过商品ID查询商品销量
    Integer getSalesByProductId(@Param("productId") String productId);
}
