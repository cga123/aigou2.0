package com.hbgc.mapper;

import com.hbgc.entity.BrandLQ;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author
 * @Date2021/4/29 13:33
 * @Version V1.0
 * 品牌管理
 **/

public interface BrandLQMapper {

    @Select("SELECT sp.id AS id,sp.product_name AS product_name,sp.product_image AS product_image,sp.price AS price," +
            "st.product_type_name AS product_type_name,sb.brand_name AS brand_name ,sp.create_time AS create_time" +
            " FROM s_product sp,s_brand sb,s_product_type st WHERE sp.product_brand = sb.id AND st.id = sb.brand_type")
    List<BrandLQ> sleBrandAll();

    @Select("SELECT sp.id AS id,sp.product_name AS product_name,sp.product_image AS product_image,sp.price AS price," +
            "st.product_type_name AS product_type_name,sb.brand_name AS brand_name ,sp.create_time AS create_time" +
            " FROM s_product sp,s_brand sb,s_product_type st WHERE sp.product_brand = sb.id AND st.id = sb.brand_type AND st.product_type_name = #{name}")
    List<BrandLQ> selBrandByTypeName(String name);

    //2.删除
    @Delete("DELETE FROM s_product WHERE id = #{id} ")
    int deleteBrandById(String id);

    @Select("SELECT sp.id,sp.product_name,sp.product_image,sp.price,st.product_type_name,sb.brand_name,sp.create_time FROM s_product sp,s_brand sb,s_product_type st WHERE sp.product_brand = sb.id AND st.id = sb.brand_type AND sp.id = #{id}")
    BrandLQ selBrandById(String id);

    @Update("UPDATE s_product SET product_name = #{product_name},product_image = #{product_image},price = #{price} WHERE id = #{id}")
    int UpdateBrandByID(@Param("product_name") String product_name, @Param("product_image") String product_image, @Param("price") float price, @Param("id") String id);


//    int addProduct(BrandLQ brandLQ);
    //查询name
    @Select("SELECT  st.product_type_name AS product_type_name\n" +
            "            FROM s_product sp,s_brand sb,s_product_type st WHERE sp.product_brand = sb.id AND st.id = sb.brand_type ")
    List<BrandLQ> AllByname();
    //通过name查询
     @Select("SELECT sp.id AS id,sp.product_name AS product_name,sp.product_image AS product_image,sp.price AS price,\n" +
             "            st.product_type_name AS product_type_name,sb.brand_name AS brand_name ,sp.create_time AS create_time\n" +
             "            FROM s_product sp,s_brand sb,s_product_type st WHERE sp.product_brand = sb.id AND st.id = sb.brand_type \n" +
             "            AND product_type_name=#{productTypeName}")
    List<BrandLQ> Allbynames(@Param("productTypeName") String productTypeName);




}
