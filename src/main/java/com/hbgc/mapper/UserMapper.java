package com.hbgc.mapper;

import com.hbgc.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    //查询用户
    @Select("select * from s_user limit #{start}, #{size}")
    public List<User> findAll(Map<String, Object> map);

    //删除用户
    @Delete("delete from  s_user where id = #{id}")
    public int userdelete(String id);

    //增加用户
    @Insert("insert into s_user values(#{user.id}, #{user.username}, #{user.password}, #{user.type})")
    public int adduser(@Param("user") User user);

    //根据id查询
    @Update("update s_user set username = #{user.username}, password = #{user.password}, type = #{user.type} where id = #{user.id}")
    public int updateUserById(@Param("user") User user);

    /**
     * 根据用户名和id查询用户
     */
    public User getUserByName(User user);


    //根据id回显数据
    @Select("select * from  s_user where id = #{id}")
    public User getUserById(String id);
}
