package com.hbgc.mapper;

import com.hbgc.entity.Product;
import org.apache.ibatis.annotations.Param;

//商品类别持久层接口
public interface ProductMapper {
    /**
     * 通过id获取商品
     * @param id
     * @return
     */
    Product getProductById(@Param("id") String id);

}
