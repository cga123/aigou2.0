package com.hbgc.mapper;

import com.hbgc.entity.ReceivingAddress;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface ReceiveingAddressMapper {

    @Select("SELECT id,receiving_address,receiving_person,mobile_phone,user_id,is_default " +
            "FROM s_receiving_address WHERE user_id = #{id}")
    List<ReceivingAddress> findAllAddById(String id);

    @Insert("INSERT INTO s_receiving_address (id, receiving_address, receiving_person, mobile_phone, user_id, is_default)" +
            " VALUES (#{id},#{receivingAddress},#{receivingPerson},#{mobilePhone},#{userId},#{isDefault})")
    int addReceivingAddress(ReceivingAddress receivingAddress);


    @Update("UPDATE s_receiving_address SET is_default = -1 WHERE user_id = #{id}")
    void updateNoDef(String id);

    @Update("UPDATE s_receiving_address SET is_default = '1' WHERE id = #{id}")
    void updateDef(String id);

    @Update("UPDATE s_receiving_address SET  receiving_address=#{receivingAddress} , receiving_person =#{receivingPerson}, \n" +
            "mobile_phone=#{mobilePhone} ,user_id=#{userId},is_default= #{isDefault}  WHERE id = #{id}")
    int updateReceivingAddress(ReceivingAddress receivingAddress);


    @Delete("DELETE FROM s_receiving_address WHERE id = #{id}")
    void delete(String id);
}
