package com.hbgc.mapper;

import com.hbgc.entity.Product;
import com.hbgc.entity.ProductType;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

//全球进口
@Mapper
public interface ProductTypeMapper {
    //分页查询
    @Select("select * from s_product_type limit #{start},#{size}")
    List<ProductType> findByAll(Map<String, Object> map);
    //通过类别信息查询商品
//    List<Product> getProductsByType(Product product);
    //通过类别信息查询商品
    List<Product> getProductsByType(Map<String,Object> map);
    //新品
    List<Product> getNewProducts();
    //排行榜
    List<Product> getProductRankings();

    @Delete("delete from s_product_type where id=#{id}")
    int del(String id);

    @Select("select * from s_product_type where id=#{id}")
    ProductType findById(String id);

    //DEBUG [http-nio-8080-exec-3] - ==> Parameters: 全球进口(String), 全球进(String), null, null
    @Update("update s_product_type set product_type_name = #{productTypeName}, product_type_desc = #{productTypeDesc} where id = #{id}")
    int udpdate(ProductType productType);

    @Insert("insert into s_product_type values(#{id},#{productTypeName},#{productTypeDesc},#{productTypeIcon} ) ")
    int add(ProductType productType);

    @Select("select * from s_product_type")
    List<ProductType> findByAllB();


}
