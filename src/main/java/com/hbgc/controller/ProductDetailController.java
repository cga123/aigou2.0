package com.hbgc.controller;

import com.hbgc.entity.Product;
import com.hbgc.service.IOrderProductService;
import com.hbgc.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 商品详细信息控制层
 */
@Controller
@RequestMapping("/front/product_detail")
public class ProductDetailController {
    /**
     * 业务层接口
     */
     @Autowired
     private IProductService productService;
     @Autowired
     private IOrderProductService orderProductService;
    /**
     * 通过id查询商品
     * @return
     */
    @RequestMapping("/productDetail")
    public String productDetail(@RequestParam("id") String id, Model model){
        // 通过id查询商品
        Product product = productService.getProductById(id);
        //查询商品销量
        Integer sales= orderProductService.getSalesByProductId(product.getId());
        product.setSales(sales);
        model.addAttribute("product",product);
        return "front/product_detail/product_detail";
    }
}
