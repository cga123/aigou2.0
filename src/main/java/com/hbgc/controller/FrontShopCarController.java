package com.hbgc.controller;

import com.hbgc.entity.Product;
import com.hbgc.entity.ReceivingAddress;
import com.hbgc.entity.ShopCartProduct;
import com.hbgc.entity.User;
import com.hbgc.ocnstant.Constants;
import com.hbgc.service.FrontShopCarService;
import com.hbgc.utils.Page;

import com.hbgc.entity.*;
import com.hbgc.ocnstant.Constants;
import com.hbgc.service.FrontShopCarService;
import com.hbgc.utils.Page;
import com.hbgc.utils.UUIDUtils;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sun.rmi.runtime.Log;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/car")
@Controller
public class FrontShopCarController {

    @Autowired
    private FrontShopCarService frontShopCarService;

    /**
     * 添加购物车
     */
    @ResponseBody
    @RequestMapping("addProductToCart")
    public Map<String,String> addProductToCart(HttpServletRequest request,
                                               @RequestParam("product_id") String product_id,
                                               @RequestParam("product_num") String product_num){
        Map<String,String>map = new HashMap<>();
        Object object = request.getSession().getAttribute(Constants.USER_SESSION_CODE);
        //用户未登录
        if (object==null){
            map.put("message","请登录");
            return map;
        }
        User user = (User) object;
        ShopCar shopCart = frontShopCarService.getShopCartByUserId(user.getId());
        //没有购物车,创建一个购物车
        if (shopCart==null) {
            shopCart = new ShopCar();
            shopCart.setId(UUIDUtils.getId());
            shopCart.setCartId(UUIDUtils.getId());
            shopCart.setUserId(user.getId());
        }
        //设置购物车详情
        ShopCartProduct shopCartProduct=new ShopCartProduct();
        shopCartProduct.setId(UUIDUtils.getId());
        shopCartProduct.setProductId(product_id);
        shopCartProduct.setProductNum(Integer.parseInt(product_num));
        shopCartProduct.setShopCartId(shopCart.getId());
        frontShopCarService.addShopCart(shopCart,shopCartProduct);
        map.put("result", "200");
        return map;
    }

    /**
     * 跳转购物车页面
     * @param session
     * @param model
     * @return
     */
    @RequestMapping("/toShopCar")
    public String toShopCar(HttpSession session, Model model){
        User user = (User) session.getAttribute(Constants.USER_SESSION_CODE);
        String userid = (String)user.getId();
        //获得收获地址
        List<ReceivingAddress> addr = frontShopCarService.findAddsByUserId(userid);

        //查询购物车id
        String carId = frontShopCarService.findCarIdByUserId(userid);


        //查询购物车所有产品
        List<ShopCartProduct> shopCartProductList = frontShopCarService.findCartProductByCarId(carId);


        for (ShopCartProduct shopCartProduct : shopCartProductList) {
            String productId = shopCartProduct.getProductId();

            Product product = frontShopCarService.findProductByPId(productId);
            shopCartProduct.setProduct(product);
        }

        model.addAttribute("list",shopCartProductList);
        model.addAttribute("address",addr);
        return "front/shop_cart/shop_cart";
    }

    /**
     * 删除购物车单项商品
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Page delete( String id){
        int i = frontShopCarService.deleteProductById(id);
        Page page = new Page();
        if(i>0){
            page.setCode("200");
            page.setMsg("已删除成功");
            return page;
        }
        page.setCode("0");
        page.setMsg("删除失败");
        return page;
    }

    /**
     * 删除购物车多项商品
     */
    @RequestMapping(value = "/deleteByAll",method = RequestMethod.POST)
    @ResponseBody
    public Page deleteByAll(@RequestBody List<String>ids){
        int i = 0;
        if(ids!=null){
            i= frontShopCarService.deleteProductAllById(ids);
        }

       Page page = new Page();
       if(i>0){
            page.setCode("200");
            page.setMsg("已删除成功");
            return page;
        }
        page.setMsg("删除失败");
        return page;
    }
}
