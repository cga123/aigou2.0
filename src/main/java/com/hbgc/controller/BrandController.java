package com.hbgc.controller;

import com.hbgc.entity.Brand;
import com.hbgc.entity.ProductType;
import com.hbgc.service.IBrandService;
import com.hbgc.service.IProductTypeService;
import com.hbgc.utils.Page;
import com.hbgc.utils.ResponseEntity;
import com.hbgc.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private IBrandService brandService;

    @Autowired
    private IProductTypeService productTypeService;

    @ResponseBody
    @RequestMapping("/upload")
    public ResponseEntity upload(MultipartFile file) throws IOException {
        System.out.println(123);
        String filename = file.getOriginalFilename();
        //System.out.println("文件名字："+filename);
        String suffix = filename.substring(filename.lastIndexOf("."), filename.length());
        String id = UUID.randomUUID().toString().replace("-", "");
        String newFileName = id + suffix;
        System.out.println(newFileName);
        file.transferTo(new File("D://aigou//upload//eshop//"+newFileName));
        return ResponseEntity.success(newFileName);
    }

    /**
     * gnggai
     * @param brand
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Map<String,Object> update(Brand brand){

         int res = brandService.update(brand);
        Map<String, Object> map = new HashMap<>();
        if(res!=0) {
            map.put("code", 0);
            map.put("msg", "success");
        } else {
            map.put("code", -1);
            map.put("msg", "fail");
        }
        return map;
    }

    @RequestMapping("/add")
    @ResponseBody
    public Map<String,Object> add(Brand brand){
        brand.setId(UUIDUtils.getId());
        System.out.println("-------------------------------------------");
        System.out.println(brand.toString());
        int res = brandService.add(brand);
        Map<String, Object> map = new HashMap<>();
        if(res!=0) {
            map.put("code", 0);
            map.put("msg", "success");
        } else {
            map.put("code", -1);
            map.put("msg", "fail");
        }
        return map;
    }

    @RequestMapping("/addPage")
    public String addPage( Model model){
        List<ProductType> productTypes = productTypeService.findByAllB();
        model.addAttribute("productTypes",productTypes);
        return "/admin/brand/add";
    }

    @RequestMapping("/updatePage/{id}")
    public String updatePage(@PathVariable("id") String id, Model model){
        Brand brand =  brandService.findById(id);
        List<ProductType> productTypes = productTypeService.findByAllB();
        model.addAttribute("productTypes",productTypes);
        model.addAttribute("brand",brand);
        return "/admin/brand/update";
    }

    @RequestMapping("/page")
    public String page(){
        return "/admin/brand/brandPage";
    }


    @RequestMapping("/findByAll")
    @ResponseBody
    public Page findByAll(Integer limit, Integer page) {
        Map<String, Object> map = new HashMap<>();
        map.put("start", (page - 1) * limit);
        map.put("size", limit);
        List<Brand> lists = brandService.findByAll(map);

        Page p = new Page();
        p.setData(lists);
        p.setCount(10);
        return p;
    }


    @RequestMapping("/del/{id}")
    @ResponseBody
    public Map<String, Object> del(@PathVariable("id") String id){

        int res= brandService.del(id);
        if(res!=0){
            System.out.println("删除成功");
        }
        Map<String, Object> map = new HashMap<>();

        if(res!=0) {
            map.put("code", 0);
            map.put("msg", "success");
        } else {
            map.put("code", -1);
            map.put("msg", "fail");
        }
        return map;
    }

}
