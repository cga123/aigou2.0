package com.hbgc.controller;

import com.hbgc.entity.ReceivingAddress;
import com.hbgc.entity.User;
import com.hbgc.ocnstant.Constants;
import com.hbgc.service.ReceiveingAddressService;
import com.hbgc.utils.Page;
import com.hbgc.utils.UUIDUtils;
import com.mysql.jdbc.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;

@Controller
@RequestMapping("/rec_Address")
public class FrontReceiveingAddressController {
    @Autowired
    private ReceiveingAddressService receiveingAddressService;
    /**
     * 跳转到收获地址，并通过用户id 查询所有收获地址
     * @param session
     * @return
     */
    @RequestMapping("/toAddress")
    public String toAddress(HttpSession session, Model model){
        User user = (User) session.getAttribute(Constants.USER_SESSION_CODE);
        List<ReceivingAddress> lists = null;


        lists = receiveingAddressService.findAllAddById(user.getId());

        model.addAttribute("list",lists);

        return "/front/receiving_address/deliverAddress";
    }


    @RequestMapping("/setDefault")
    @ResponseBody
    public Page setDefault(HttpSession session,String id){
        Page page = new Page();
        User user = (User) session.getAttribute(Constants.USER_SESSION_CODE);
        receiveingAddressService.updateNoDef(user.getId());
        receiveingAddressService.updateDef(id);
        page.setCode("200");
        page.setMsg("已设为默认地址");
        return page;
    }

    @RequestMapping("/delete")
    @ResponseBody
    public Page delete(String id){
        Page page = new Page();
        receiveingAddressService.delete(id);
        page.setCode("200");
        page.setMsg("已删除成功");
        return page;
    }


    @RequestMapping("/save")
    @ResponseBody
    public Page save(HttpSession session, ReceivingAddress receivingAddress){
            Page page = new Page();

        User user = (User) session.getAttribute(Constants.USER_SESSION_CODE);
        String id = receivingAddress.getId();
        Integer def = receivingAddress.getIsDefault();
        receivingAddress.setUserId(user.getId());

        if(StringUtils.isNullOrEmpty(id)){
            receivingAddress.setId(UUIDUtils.getId());

            int i = receiveingAddressService.addReceivingAddress(receivingAddress);


            /**
             * 如果默认传过来为 1
             * 通过当前用户id吧地址默认全部改为-1
             * 在通过地址id 吧地址设为 1
             */
            if(def==1){
                receiveingAddressService.updateNoDef(user.getId());
                receiveingAddressService.updateDef(receivingAddress.getId());
            }
        }else{
            int i = receiveingAddressService.updateReceivingAddress(receivingAddress);

            if(def==1){
                receiveingAddressService.updateNoDef(user.getId());
                receiveingAddressService.updateDef(receivingAddress.getId());
            }
        }


        page.setCode("200");
        page.setMsg("地址添加/修改成功");
        return page;
    }
}
