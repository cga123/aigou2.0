package com.hbgc.controller;

import com.hbgc.entity.ProductType;
import com.hbgc.service.IProductTypeService;
import com.hbgc.utils.Page;
import com.hbgc.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/productType")
public class productTypeController {

    @Autowired
    private IProductTypeService productTypeService;

    @RequestMapping("/PTpage")
    public String PTpage(){
        return "/admin/product_type/productTypePage";
    }


    @RequestMapping("/updatePage/{id}")
    public String updatePage(@PathVariable("id") String id,Model model){
        ProductType productType =  productTypeService.findById(id);
        model.addAttribute("productType",productType);
        return "/admin/product_type/update";
    }

    @RequestMapping("/addPage")
    public String addPage(String id,Model model){

        return "/admin/product_type/add";
    }

    @RequestMapping("/update")
    @ResponseBody
    public  Map<String, Object> update(ProductType productType){

       int res= productTypeService.udpdate(productType);
        Map<String, Object> map = new HashMap<>();
       if(res!=0) {
            map.put("code", 0);
            map.put("msg", "success");
        } else {
            map.put("code", -1);
            map.put("msg", "fail");
        }
        return map;
    }


    @RequestMapping("/add")
    @ResponseBody
    public  Map<String, Object> add(ProductType productType){
        productType.setId(UUIDUtils.getId());
        productType.setProductTypeIcon("icon-systemprompt_fill");

        int res= productTypeService.add(productType);

        Map<String, Object> map = new HashMap<>();
        if(res!=0) {
            map.put("code", 0);
            map.put("msg", "success");
        } else {
            map.put("code", -1);
            map.put("msg", "fail");
        }
        return map;
    }

    @RequestMapping("/findByAll")
    @ResponseBody
    public Page findByAll(Integer limit, Integer page){
        Map<String, Object> map = new HashMap<>();
        map.put("start",(page-1)*limit);
        map.put("size",limit);
        List<ProductType> lists =  productTypeService.findByAll(map);
        Page p = new Page();
        p.setData(lists);
        p.setCount(10);
        return p;

    }

    @RequestMapping("/del/{id}")
    @ResponseBody
    public Map<String, Object> del(@PathVariable("id") String id){

        int res= productTypeService.del(id);
        if(res!=0){
            System.out.println("删除成功");
        }
        Map<String, Object> map = new HashMap<>();

        if(res!=0) {
            map.put("code", 0);
            map.put("msg", "success");
        } else {
            map.put("code", -1);
            map.put("msg", "fail");
        }
        return map;
    }

}
