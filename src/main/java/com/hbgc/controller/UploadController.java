package com.hbgc.controller;

import com.hbgc.utils.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Controller
public class UploadController {

    @ResponseBody
    @RequestMapping("upload")
    public ResponseEntity upload(MultipartFile file){
        String filename = file.getOriginalFilename();
        String suffix = filename.substring(filename.lastIndexOf("."), filename.length());
        String id = UUID.randomUUID().toString().replace("-", "");
        String newFileName = id + suffix;

        try {
            file.transferTo(new File("D://aigou//upload//eshop//" + newFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.success(newFileName);
    }
}
