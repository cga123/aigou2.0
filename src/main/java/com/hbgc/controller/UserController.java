package com.hbgc.controller;

import com.hbgc.entity.User;
import com.hbgc.service.UserService;
import com.hbgc.utils.EncryptionUtils;
import com.hbgc.utils.Page;
import com.hbgc.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
public class UserController {

    @Autowired
    UserService userService;



    /**
     * 帮助转跳列表页面
     * @return
     */
    @RequestMapping("/admin/userpage")
    public String userpage(){

        return "/admin/user/list";
    }


    /***
     * 帮助转跳增加页面
     * @return
     */
    @RequestMapping("/admin/addpage")
    public String addpage(){

        return "/admin/user/add";
    }


    /**
     * 帮助转跳修改页面
     */
    @GetMapping("/admin/updatepage/{id}")
    public String updatepage(@PathVariable("id") String id, Model model){
        User userById = userService.getUserById(id);
        System.out.println(userById);

        model.addAttribute("user", userById);
        return "/admin/user/update";
    }

    /**
     * 查询
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin/pagelist")
    public Page UserList(Integer limit, Integer page){

        Map<String, Object> map = new HashMap<>();
        map.put("start",(page - 1)*limit);
        map.put("size", limit);

        List<User> users = userService.findAll(map);
        Page p = new Page();
        p.setCode("0");
        p.setCount(users.size());
        p.setMsg("success");
        p.setData(users);

        return p;


    }


    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping(value = "/admin/userdelete",method = RequestMethod.GET)
    @ResponseBody
    public int userdelete(String id){
        System.out.println("===="+id);
        int result = userService.userdelete(id);
        //Map<String, Object> map = new HashMap<>();
        
        return result;
    }


    /**
     * 增加
     * @param user
     * @return
     */
    @RequestMapping("/admin/addtuser")
    @ResponseBody()
    public  Map<String, Object> adduser(User user)
    {
        System.out.println(user.toString());
        String uid= UUIDUtils.getId();
        String a= EncryptionUtils.encryptMD5(uid);
        user.setId(a);
        String upwd = user.getPassword();
        String p = EncryptionUtils.encryptMD5(upwd);
        user.setPassword(p);
        int res = userService.adduser(user);

        Map<String, Object> map = new HashMap<>();
        if(res!=0) {
            map.put("code", 0);
            map.put("msg", "success");
        } else {
            map.put("code", -1);
            map.put("msg", "fail");
        }
        return map;
    }



    /**
     * 修改
     */

    @ResponseBody
    @RequestMapping("/admin/updateuser")
    public int updatauser(User user){

        int userById = userService.updateUserById(user);
        System.out.println("=====update"+userById);
        if(userById > 0)
        {
            return userById;
        }

        return -1;
    }

}
