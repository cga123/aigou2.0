package com.hbgc.controller;

import com.hbgc.entity.Brand;
import com.hbgc.entity.BrandLQ;
import com.hbgc.entity.ProductType;
import com.hbgc.entity.result;
import com.hbgc.mapper.BrandLQMapper;
import com.hbgc.mapper.BrandMapper;
import com.hbgc.mapper.ProductTypeMapper;
import com.hbgc.utils.Page;
import com.hbgc.utils.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author LQ
 * @Date2021/4/29 13:37
 * @Version V1.0
 **/
@Controller
@RequestMapping("pp")
public class BrandControllerLQ {

    @Autowired
    private BrandLQMapper ppMapper;

    @Autowired
    private BrandMapper brandMapper;

    @Autowired
    private ProductTypeMapper productTypeMapper;

    @RequestMapping("add")
    public Page add(BrandLQ brandLQ){


        Page page=new Page();
            page.setCode("0");
            page.setMsg("success");

        return  page;
    }

    @RequestMapping("pppage")
    public String test(Model model){
        List<BrandLQ> brandLQS = ppMapper.AllByname();
        model.addAttribute("brandLQS",brandLQS);
        return "/admin/product/pp";
    }

    @RequestMapping("addpage")
    public String test1(Model model){
        List<Brand> brands = brandMapper.findByAllA();
        List<ProductType> productTypes = productTypeMapper.findByAllB();
        model.addAttribute("brands",brands);
        model.addAttribute("productTypes",productTypes);
        return "/admin/product/add";
    }

    @ResponseBody
    @GetMapping("/page")
    public Page page(result result){
        List<BrandLQ> brands = new ArrayList<>();
        brands = ppMapper.sleBrandAll();
        if(result.getKey()!=null){
            String type =  String.valueOf(result.getKey().get("productTypeName"));
            System.out.println("狗日的不为空");
            if(type!=null && !type.equals("--无--")){
                brands =  ppMapper.selBrandByTypeName(type);
            }else{
                brands = ppMapper.sleBrandAll();
            }
        }else{
            System.out.println("狗日的为空");
        }
        System.out.println("执行了查询全部");
        Page pages = new Page();
       if(brands==null){
       }else {
           pages.setData(brands);
           pages.setCount(brands.size());
       }
        return pages;
    }


    //根据条件查询
    @ResponseBody
    @RequestMapping("/tjall")
     public Page tjall(String productTypeName){
        System.out.println(productTypeName);
         List<BrandLQ> allbynames = ppMapper.Allbynames(productTypeName);
        System.out.println(123);
         System.out.println(allbynames);
         Page page = new Page();
         page.setData(allbynames);
         page.setCount(allbynames.size());
         return page;
     }
    @GetMapping("update/{id}")
    public String update(@PathVariable("id") String id, Model model){
        System.out.println("回调："+id);
        BrandLQ brandLQ = ppMapper.selBrandById(id);
        model.addAttribute("brandLQ", brandLQ);
        return "/admin/product/updatepp";
    }

    @ResponseBody
    @GetMapping("/delete/{id}")
    public int delete(@PathVariable String id){
        System.out.println("接到需要删除的商品：" + id);
        int i = ppMapper.deleteBrandById(id);
        return i;
    }

    @ResponseBody
    @PostMapping("update")
    public ResponseEntity update(BrandLQ brandLQ) throws UnsupportedEncodingException {
        ppMapper.UpdateBrandByID(brandLQ.getProductName(),brandLQ.getProductImage(),brandLQ.getPrice(),brandLQ.getId());
        return ResponseEntity.success();
    }




}
