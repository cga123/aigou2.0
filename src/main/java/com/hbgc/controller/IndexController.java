package com.hbgc.controller;

import com.hbgc.entity.CarouselFigure;
import com.hbgc.entity.Product;
import com.hbgc.entity.ProductType;
import com.hbgc.service.ICarouselFigureService;
import com.hbgc.service.IProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//首页控制器
@Controller
@RequestMapping("front")
public class IndexController {
    @Autowired
    private ICarouselFigureService service;
    @Autowired
    private IProductTypeService Pservice;
    @RequestMapping("index")
    public String IndexShow(Model model){
    //轮播图
        List<CarouselFigure> allcarouselFigures = service.getAllcarouselFigure();
        //新品
        List<Product> newProducts = Pservice.getNewProducts();
        //排行榜
        List<Product> rankings = Pservice.getProductRankings();
        //全球进口
        ProductType productType = new ProductType();
        //商品类别名称
        String ProductTypeName="全球进口";
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("ProductTypeName",ProductTypeName);
        map.put("num",5);
        List<Product> list = Pservice.getProductsByType(map);
        //服装服饰
        //商品类别名称
         ProductTypeName="服装服饰";
         map.put("ProductTypeName",ProductTypeName);
         map.put("num",12);
         List<Product> list2 = Pservice.getProductsByType(map);
         //护肤美妆
         //商品类别名称
         ProductTypeName="护肤美妆";
         map.put("ProductTypeName",ProductTypeName);
         map.put("num",5);
         List<Product> list3 = Pservice.getProductsByType(map);
        //图书学习
        //商品类别名称
        ProductTypeName="图书学习";
        map.put("ProductTypeName",ProductTypeName);
        map.put("num",12);
        List<Product> list4 = Pservice.getProductsByType(map);

        model.addAttribute("allcarouselFigures", allcarouselFigures);
        model.addAttribute("newProducts",newProducts);
        model.addAttribute("rankings",rankings);
        model.addAttribute("list",list);
        model.addAttribute("list2",list2);
        model.addAttribute("list3",list3);
        model.addAttribute("list4",list4);
      return "front/index/index";
    }
}
