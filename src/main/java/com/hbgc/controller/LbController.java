package com.hbgc.controller;

import com.hbgc.entity.CarouselFigure;
import com.hbgc.service.ICarouselFigureService;
import com.hbgc.utils.Page;
import com.hbgc.utils.ResponseEntity;
import com.hbgc.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("Lb")
public class LbController {
    @RequestMapping("Lbpage")
    public String Lbpage(){
        return "/carousel_figure/lb";
    }
    @Autowired
  private ICarouselFigureService lbService;
    /*
    查询所有
     */
    @ResponseBody
    @RequestMapping("page")
    public Page page(){
        List<CarouselFigure> list = lbService.getAllcarouselFigure();
        Page page=new Page();
        page.setData(list);
        page.setCount(list.size());
        return page;
    }
    /**
     * 通过id获取轮播图信息
     */
    @RequestMapping("update/{id}")
    public String update(@PathVariable("id") String id, Model model){
        CarouselFigure carouselFigure = lbService.getById(id);
        model.addAttribute("carouselFigure", carouselFigure);
        return  "/carousel_figure/update";
    }
    //修改轮播图
    @ResponseBody
    @PostMapping("update")
    public ResponseEntity update(CarouselFigure carouselFigure){
        lbService.update(carouselFigure);
        return ResponseEntity.success();
    }
    //上传图片文件
    @ResponseBody
    @RequestMapping("upload")
    public ResponseEntity upload(MultipartFile file) throws IOException {
        System.out.println(123);
        String filename = file.getOriginalFilename();
        //System.out.println("文件名字："+filename);
        String suffix = filename.substring(filename.lastIndexOf("."), filename.length());
        String id = UUID.randomUUID().toString().replace("-", "");
        String newFileName = id + suffix;
        //System.out.println(newFileName);
        file.transferTo(new File("D://aigou//upload//eshop//"+newFileName));
        return ResponseEntity.success(newFileName);
    }
    //删除轮播图
    @ResponseBody
    @RequestMapping("del/{id}")
    public Page del(@PathVariable("id") String id){
        int del = lbService.del(id);
        System.out.println("shanchuchnegg");
        Page page=new Page();
        if (del!=0){
            page.setCode("0");
            page.setMsg("success");
        }
        else {
            page.setCode("-1");
            page.setMsg("fail");
        }
        return  page;
    }
    //添加
    @RequestMapping("/addPage")
    public String addPage(){

        return "/carousel_figure/add";
    }

    //添加轮播图
    @RequestMapping("/add")
    @ResponseBody
     public Page add(CarouselFigure carouselFigure){
        System.out.println("wojinliale");
        carouselFigure.setId(UUIDUtils.getId());
        System.out.println(UUIDUtils.getId());
        System.out.println(carouselFigure);
         int add = lbService.add(carouselFigure);
         Page page=new Page();
        if (add!=0){
            page.setCode("0");
            page.setMsg("success");
        }
        else {
            page.setCode("-1");
            page.setMsg("fail");
        }
         return  page;
     }

}
