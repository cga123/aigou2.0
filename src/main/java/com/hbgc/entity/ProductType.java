package com.hbgc.entity;

import lombok.Data;

@Data
public class ProductType {

	private String id;

	private String productTypeName;

	private String productTypeDesc;
	
	private String productTypeIcon;


}