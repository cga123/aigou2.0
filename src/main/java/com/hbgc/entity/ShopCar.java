package com.hbgc.entity;

import lombok.Data;

@Data
public class ShopCar {
    private String id;

    private String cartId;

    private String userId;
}
