package com.hbgc.entity;

import lombok.Data;

@Data
public class ReceivingAddress {

    private String id;

    private String receivingAddress;

    private String receivingPerson;

    private Long mobilePhone;

    private String userId;

    private Integer isDefault;
}
