package com.hbgc.entity;

import lombok.Data;

//伦博徒
@Data
public class CarouselFigure {

	private String id;

	private String url;

	private Integer sequenceNum;

}