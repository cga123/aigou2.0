package com.hbgc.entity;

import lombok.Data;

/**
 * @Author
 * @Date2021/4/29 13:34
 * @Version V1.0
 *
 **/
@Data
public class BrandLQ {
    private String id;
    private String productName;
    private String productImage;
    private float price;
    private String productTypeName;
    private String brandName;
    private String createTime;
}
