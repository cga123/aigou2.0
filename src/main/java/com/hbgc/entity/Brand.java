package com.hbgc.entity;

import lombok.Data;

@Data
public class Brand {

	private String id;

	private String brandName;

	private String brandType;
	
	private String brandImg;


}