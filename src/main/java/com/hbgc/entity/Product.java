package com.hbgc.entity;

import lombok.Data;
@Data
public class Product {

	private String id;

	private String productName;

	private String productImage;

	private double price;
	
	private String productDesc;

	private ProductType productType;
	
	private String createTime;
	
	private Brand productBrand;
	
	private int sales;

}