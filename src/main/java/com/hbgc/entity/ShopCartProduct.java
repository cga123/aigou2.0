package com.hbgc.entity;

import lombok.Data;

@Data
public class ShopCartProduct {

    private String id;

    private String shopCartId;

    private String productId;

    private Integer productNum;

    private Product product;

}
