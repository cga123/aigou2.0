package com.hbgc.entity;

import lombok.Data;

import java.util.Map;
@Data
public class result {
    private int page;
    private int limit;
    private Map<Object,Object>key;
    /*page: 1
    limit: 10
    key[productTypeName]: 全球进口*/

    public result(){
       this.page = 0;
       this.limit = 10;
       this.key = null;
    }

}
