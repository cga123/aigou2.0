package com.hbgc.entity;

import lombok.Data;

/**
 * 订单销量表
 */
@Data
public class OrderProduct {
    private String id;

    private String orderId;

    private String productId;

    private Integer productNum;

    private Product product;
}
