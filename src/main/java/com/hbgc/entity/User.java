package com.hbgc.entity;

import lombok.Data;

/**
 * author: 周健
 * time :2021/4/28/9.40
 *
 * 用户类
 */

@Data
public class User {

    private String id;
    private String username;
    private String password;
    private int type;


}
