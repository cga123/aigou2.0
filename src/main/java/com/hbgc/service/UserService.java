package com.hbgc.service;

import com.hbgc.entity.User;

import java.util.List;
import java.util.Map;

/**
 *  author: 周健
 *  time :2021/4/28/9.40
 *
 *  操作用户的service接口
 */

public interface UserService {


    //查询用户
    List<User> findAll(Map<String, Object> map);

    //删除用户
    int userdelete(String id);

    //增加用户
    int adduser(User user);

    //更新用户
    int updateUserById(User user);

    /**
     * 根据用户名和id查询用户
     */
    User getUserByName(User user);


    //根据id回显数据
    User getUserById(String id);
}


