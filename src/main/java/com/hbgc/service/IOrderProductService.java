package com.hbgc.service;

import org.apache.ibatis.annotations.Param;

/**
 * 商品销量业务层接口
 */
public interface IOrderProductService {
    //通过商品ID查询商品销量
    Integer getSalesByProductId(@Param("productId") String productId);
}
