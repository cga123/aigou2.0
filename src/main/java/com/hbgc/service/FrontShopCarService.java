package com.hbgc.service;

import com.hbgc.entity.Product;
import com.hbgc.entity.ReceivingAddress;
import com.hbgc.entity.ShopCar;
import com.hbgc.entity.ShopCartProduct;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FrontShopCarService {

    List<ReceivingAddress> findAddsByUserId(String userid);

    String findCarIdByUserId(String userid);

    List<ShopCartProduct> findCartProductByCarId(String carId);

    Product findProductByPId(String productId);

    int deleteProductById(String id);

    int deleteProductAllById(List<String> ids);
    //通过用户ID查询购物车
    ShopCar getShopCartByUserId(@Param("userId") String userId);

    /**
     *添加购物车
     * @param shopCart
     * @param shopCartProduct
     */
    void addShopCart(ShopCar shopCart, ShopCartProduct shopCartProduct);
}
