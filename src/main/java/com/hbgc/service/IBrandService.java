package com.hbgc.service;

import com.hbgc.entity.Brand;

import java.util.List;
import java.util.Map;

public interface IBrandService {
    List<Brand> findByAll(Map<String, Object> map);

    int del(String id);

    Brand findById(String id);

    int update(Brand brand);

    int add(Brand brand);
}
