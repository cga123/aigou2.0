package com.hbgc.service;

import com.hbgc.entity.Product;
import org.apache.ibatis.annotations.Param;
//商品详细业务层接口
public interface IProductService {
    /**
     * 通过id获取商品
     * @param id
     * @return
     */
    Product getProductById(@Param("id") String id);
}
