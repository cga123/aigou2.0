package com.hbgc.service;

import com.hbgc.entity.CarouselFigure;
import org.apache.ibatis.annotations.Param;

import java.util.List;
//业务层接口
public interface ICarouselFigureService {
    //查询所有轮播图
    List<CarouselFigure> getAllcarouselFigure();
    //根据id查询
    CarouselFigure getById(@Param("id") String id);
    //更新轮播图
    void update(CarouselFigure carouselFigure);
    //删除轮播图
    int del(@Param("id") String id);
    //add
    int add(CarouselFigure carouselFigure);
}
