package com.hbgc.service;

import com.hbgc.entity.Product;
import com.hbgc.entity.ProductType;

import java.util.List;
import java.util.Map;

public interface IProductTypeService {
    //排序查询
    List<ProductType> findByAll(Map<String,Object> map);
    //通过商品类别查询商品
   // List<Product> getProductsByType(Product product);
    List<Product> getProductsByType(Map<String,Object> map);
    //新品
    List<Product> getNewProducts();
    //排行榜
    List<Product> getProductRankings();

    int del(String id);

    ProductType findById(String id);

    int udpdate(ProductType productType);

    int add(ProductType productType);

    List<ProductType> findByAllB();


}
