package com.hbgc.service;

import com.hbgc.entity.ReceivingAddress;

import java.util.List;

public interface ReceiveingAddressService {
    List<ReceivingAddress> findAllAddById(String id);

    int addReceivingAddress(ReceivingAddress receivingAddress);

    void updateNoDef(String id);

    void updateDef(String id);

    int updateReceivingAddress(ReceivingAddress receivingAddress);

    void delete(String id);
}
