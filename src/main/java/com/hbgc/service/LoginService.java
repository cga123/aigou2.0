package com.hbgc.service;

import com.hbgc.entity.User;

public interface LoginService {
    User findByUsername(String username, int type);
}
