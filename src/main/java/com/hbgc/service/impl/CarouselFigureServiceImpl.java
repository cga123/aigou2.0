package com.hbgc.service.impl;

import com.hbgc.mapper.CarouselFigureMapper;
import com.hbgc.entity.CarouselFigure;
import com.hbgc.service.ICarouselFigureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
//轮播图业务层接口实现类
@Service
public class CarouselFigureServiceImpl implements ICarouselFigureService {
    @Autowired
   private CarouselFigureMapper mapper;
    @Override
    //
    public List<CarouselFigure> getAllcarouselFigure() {
        return mapper.getAllcarouselFigure();
    }

    @Override
    public CarouselFigure getById(String id) {
        return mapper.getById(id);
    }

    @Override
    public void update(CarouselFigure carouselFigure) {
        mapper.update(carouselFigure);
    }

    @Override
    public int del(String id) {
        return mapper.del(id);
    }

    @Override
    public int add(CarouselFigure carouselFigure) {
        return mapper.add(carouselFigure);
    }
}
