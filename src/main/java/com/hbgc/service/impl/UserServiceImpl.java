package com.hbgc.service.impl;

import com.hbgc.entity.User;
import com.hbgc.mapper.UserMapper;
import com.hbgc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 *  author: 周健
 *  time :2021/4/28/9.40
 *
 *  用户service的实现
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper usermapper;

    @Override
    public List<User> findAll(Map<String, Object> map) {

        return usermapper.findAll(map);
    }

    @Override
    public int userdelete(String id){

        return usermapper.userdelete(id);
    }

    @Override
    public int adduser(User user){

        int a=usermapper.adduser(user);
        System.out.println(a);
        return a;
    }
    //根据用户查找姓名
    @Override
    public User getUserByName(User user) {
        System.out.println(123456);
        return usermapper.getUserByName(user);
    }

    //更新数据
    public int updateUserById(User user){
        return usermapper.updateUserById(user);
    }

    //回显数据
    public User getUserById(String id) {

        return usermapper.getUserById(id);
    }
}
