package com.hbgc.service.impl;

import com.hbgc.entity.User;
import com.hbgc.mapper.LoginMapper;
import com.hbgc.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginMapper loginMapper;

    @Override
    public User findByUsername(String username, int type) {

        return loginMapper.findByUsername(username,type);
    }
}
