package com.hbgc.service.impl;

import com.hbgc.mapper.OrderProductMapper;
import com.hbgc.service.IOrderProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 商品销量业务层实现类
 */
@Service
public class OrderProductServiceImpl implements IOrderProductService {
    //自动注入持久层接口
    @Autowired
    private OrderProductMapper orderProductMapper;
    //通过商品id查询销量
    @Override
    public Integer getSalesByProductId(String productId) {
        return orderProductMapper.getSalesByProductId(productId);
    }
}
