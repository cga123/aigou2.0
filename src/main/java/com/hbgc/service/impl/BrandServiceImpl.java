package com.hbgc.service.impl;

import com.hbgc.entity.Brand;
import com.hbgc.mapper.BrandMapper;
import com.hbgc.service.IBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BrandServiceImpl implements IBrandService {

    @Autowired
    private BrandMapper brandMapper;


    @Override
    public List<Brand> findByAll(Map<String, Object> map) {
        return brandMapper.findByAll(map);
    }

    @Override
    public int del(String id) {
        return brandMapper.del(id);
    }

    @Override
    public Brand findById(String id) {
        return brandMapper.findById(id);
    }

    @Override
    public int add(Brand brand) {
        return brandMapper.add(brand);
    }

    @Override
    public int update(Brand brand) {
        return brandMapper.update(brand);
    }
}
