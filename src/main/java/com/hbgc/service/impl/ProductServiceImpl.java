package com.hbgc.service.impl;

import com.hbgc.entity.Product;
import com.hbgc.mapper.ProductMapper;
import com.hbgc.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 商品详情业务层实现类
 */
@Service
public class ProductServiceImpl implements IProductService {
    /**
     * 根据id查询商品
     * @param id
     * @return
     */
    @Autowired
    private ProductMapper productMapper;
    @Override
    public Product getProductById(String id) {
        return productMapper.getProductById(id);
    }
}
