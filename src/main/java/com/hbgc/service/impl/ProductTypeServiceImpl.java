package com.hbgc.service.impl;

import com.hbgc.entity.Product;
import com.hbgc.entity.ProductType;
import com.hbgc.mapper.ProductTypeMapper;
import com.hbgc.service.IProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
//商品类别业务实现类
@Service
public class ProductTypeServiceImpl implements IProductTypeService {
    @Autowired
    private ProductTypeMapper mapper;
//    @Override
//    public List<Product> getProductsByType(Product product) {
//        return mapper.getProductsByType(product);
//    }
    //通过商品类别查询商品
    @Override
    public List<Product> getProductsByType(Map<String, Object> map) {
        return mapper.getProductsByType(map);
    }

    @Override
    public List<Product> getNewProducts() {
        return mapper.getNewProducts();
    }

    @Override
    public List<Product> getProductRankings() {
        return mapper.getProductRankings();
    }

    @Override
    public int del(String id) {
        return mapper.del(id);
    }

    @Override
    public ProductType findById(String id) {
        return mapper.findById(id);
    }

    @Override
    public int udpdate(ProductType productType) {
        return mapper.udpdate(productType);
    }

    @Override
    public int add(ProductType productType) {
        return mapper.add(productType);
    }

    @Override
    public List<ProductType> findByAllB() {
        return mapper.findByAllB();
    }


    @Override
    public List<ProductType> findByAll(Map<String,Object> map) {
        return mapper.findByAll(map);
    }


}
