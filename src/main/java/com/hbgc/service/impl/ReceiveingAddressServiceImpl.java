package com.hbgc.service.impl;

import com.hbgc.entity.ReceivingAddress;
import com.hbgc.mapper.ReceiveingAddressMapper;
import com.hbgc.service.ReceiveingAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReceiveingAddressServiceImpl implements ReceiveingAddressService {
    @Autowired
    private ReceiveingAddressMapper receiveingAddressMapper;

    @Override
    public List<ReceivingAddress> findAllAddById(String id) {
        List<ReceivingAddress>  lists= receiveingAddressMapper.findAllAddById(id);
        return lists;
    }

    @Override
    public int addReceivingAddress(ReceivingAddress receivingAddress) {
        int i= receiveingAddressMapper.addReceivingAddress(receivingAddress);
        return i;
    }

    @Override
    public void updateNoDef(String id) {
        receiveingAddressMapper.updateNoDef(id);
    }

    @Override
    public void updateDef(String id) {
        receiveingAddressMapper.updateDef(id);
    }

    @Override
    public int updateReceivingAddress(ReceivingAddress receivingAddress) {
       int i =  receiveingAddressMapper.updateReceivingAddress(receivingAddress);

        return i;
    }

    @Override
    public void delete(String id) {
        receiveingAddressMapper.delete(id);
    }
}
