package com.hbgc.service.impl;

import com.hbgc.entity.Product;
import com.hbgc.entity.ReceivingAddress;
import com.hbgc.entity.ShopCar;
import com.hbgc.entity.ShopCartProduct;
import com.hbgc.mapper.FrontShopCarMapper;
import com.hbgc.service.FrontShopCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FrontShopCarServiceImpl implements FrontShopCarService {
    @Autowired
    private FrontShopCarMapper frontShopCarMapper;

    @Override
    public List<ReceivingAddress> findAddsByUserId(String userid) {
        List<ReceivingAddress> addrs = frontShopCarMapper.findAddsByUserId(userid);
        return addrs;
    }

    @Override
    public String findCarIdByUserId(String userid) {
        return frontShopCarMapper.findCarIdByUserId(userid);
    }

    @Override
    public List<ShopCartProduct> findCartProductByCarId(String carId) {
        return frontShopCarMapper.findCartProductByCarId(carId);
    }

    @Override
    public Product findProductByPId(String productId) {
        return frontShopCarMapper.findProductByPId(productId);
    }

    //通过用户ID查询购物车
    @Override
    public ShopCar getShopCartByUserId(String userId) {
        return frontShopCarMapper.getShopCartByUserId(userId);
    }

    @Override
    public void addShopCart(ShopCar shopCart, ShopCartProduct shopCartProduct) {

    }

    @Override
    public int deleteProductById(String id) {
        return frontShopCarMapper.deleteProductById(id);
    }

    @Override
    public int deleteProductAllById(List<String> ids) {
        return frontShopCarMapper.deleteProductAllById(ids);
    }
}
