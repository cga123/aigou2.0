package com.hbgc.utils;

import lombok.Data;

@Data
public class ResponseEntity {

    private String msg = "操作成功";
    private boolean rs = true;
    private Object data;

    public static ResponseEntity success(){
        ResponseEntity responseBody = new ResponseEntity();
        return responseBody;
    }

    public static ResponseEntity success(Object data){
        ResponseEntity responseBody = new ResponseEntity();
        responseBody.setData(data);
        return responseBody;
    }

}
