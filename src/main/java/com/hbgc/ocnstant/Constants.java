package com.hbgc.ocnstant;

public interface Constants {

	public static final String USER_SESSION_CODE = "user";
	
	public static final String ADMIN_SESSION_CODE = "_admin";
	
	public static final String SYS_CODE = "code";
}
