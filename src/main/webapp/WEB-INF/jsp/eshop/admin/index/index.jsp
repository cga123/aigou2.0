<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>layout 管理系统大布局 - Layui</title>
	<link rel="stylesheet" href="/static/plugs/layui/css/layui.css">
</head>
<body>
<div class="layui-layout layui-layout-admin">
	<div class="layui-header">
		<div class="layui-logo">XXX管理系统</div>
		<!-- 头部区域（可配合layui 已有的水平导航） -->
		<ul class="layui-nav layui-layout-right">
			<li class="layui-nav-item">
				<a href="javascript:;">
					<img src="//tva1.sinaimg.cn/crop.0.0.118.118.180/5db11ff4gw1e77d3nqrv8j203b03cweg.jpg"
						 class="layui-nav-img">
					${_admin}
				</a>
				<dl class="layui-nav-child">
					<dd><a href="">修改密码</a></dd>
					<dd><a href="${ctx}/admin/logout">退出</a></dd>
				</dl>
			</li>
			<li class="layui-nav-item"><a href="${ctx}/admin/logout">退出</a></li>
		</ul>
	</div>

	<div class="layui-side layui-bg-black">
		<div class="layui-side-scroll">
			<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
			<ul class="layui-nav layui-nav-tree" lay-filter="test">
				<li class="layui-nav-item layui-nav-itemed">
					<a class="" href="javascript:;">系统管理</a>
					<dl class="layui-nav-child menu">
						<dd><a href="javascript:;" data="/admin/userpage">用户管理</a></dd>
						<dd><a href="javascript:;" data="/Lb/Lbpage">轮播图维护</a></dd>
						<dd><a href="javascript:;" data="/productType/PTpage">商品分类维护</a></dd>
						<dd><a href="javascript:;" data="/brand/page">商品品牌维护</a></dd>
						<dd><a href="javascript:;" data="/pp/pppage">商品维护</a></dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>

	<div class="layui-body">
		<div style="padding: 15px;" id="blb_body">



		</div>
	</div>

</div>

<script src="/static/plugs/layui/layui.js"></script>
<script>
	//JavaScript代码区域
	layui.use(['element', 'jquery'], function () {
		var element = layui.element;
		var $ = layui.jquery;
		$('#blb_body').load("/admin/userpage");
		//菜单点击事件
		$('.menu>dd').click(function () {
			var url = $(this).find("a:eq(0)").attr("data");

			$('#blb_body').load(url);
		})

	});
</script>
</body>
</html>