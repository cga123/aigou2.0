<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2021/4/28
  Time: 14:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Title</title>
	<link rel="stylesheet" href="/static/plugs/layui/css/layui.css">
</head>
<body style="margin: 10px">

<form class="layui-form" action="">

	<div class="layui-form-item">
		<label class="layui-form-label">分类名称</label>
		<div class="layui-input-block">
			<input class="layui-input" type="text" name="productTypeName">
		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label">分类描述</label>
		<div class="layui-input-block">
			<input class="layui-input" type="text" name="productTypeDesc" >
		</div>
	</div>

	<div class="layui-form-item">
		<div class="layui-input-block">
			<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
			<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		</div>
	</div>
</form>

<script src="/static/plugs/layui/layui.js"></script>
<script>
	layui.use(['form', 'upload', 'jquery'], function () {
		var form = layui.form;
		var upload = layui.upload;
		var $ = layui.jquery;

		form.on('submit(formDemo)', function (data) {
			console.log(data.field);

			//发送ajax请求更新数据
			$.post('/productType/add', data.field, function (d) {
				console.log(d);
				parent.layer.msg('添加成功', {icon: 1});
				parent.layui.table.reload("demo");

				//当你在iframe页面关闭自身时
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index); //再执行关闭
			})
			return false;
		});


	});
</script>
</body>
</html>
