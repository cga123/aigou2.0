<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
    <title>Title</title>
    <link href="/static/plugs/layui/css/layui.css">
</head>
<body>

<table id="demo" lay-filter="test"></table>

<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="add">添加</button>
        <button class="layui-btn layui-btn-sm" lay-event="deleteAll">删除</button>
    </div>
</script>

<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="update">修改</a>

    <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="delete">删除</a>
</script>


<script src="/static/plugs/layui/layui.js"></script>
<script src="/static/plugs/jquery.js"></script>


<script>
    layui.use(['table'], function () {
        var table = layui.table;

        //执行渲染
        table.render({
            elem: '#demo'
            , url: '/admin/pagelist' //数据接口
            , title: '用户表'
            ,toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
            ,page: true //是否显示分页
            ,limit: 10 //每页默认显示的数量
            , cols: [[ //表头
                {type: 'checkbox', fixed: 'left'}
                , {field: 'id', title: 'ID'}
                , {field: 'username', title: '用户名'}
                , {field: 'password', title: '密码'}
                , {field: 'type', title: '类型'}
                , {fixed: 'right', align: 'center', toolbar: '#barDemo'}
            ]],

            callback: function(current) {
                $('.hp-context').load("${ctx}/admin/productType/list?pageNo=" + current);
            }
        });

        //头工具栏事件
        table.on('toolbar(test)', function(obj){

            switch(obj.event) {
                //增加用户
                case 'add':
                    layer.open({
                        title: "添加用户",
                        type: 2,
                        area: ['600px', '300px'],
                        fixed: false, //不固定
                        maxmin: true,
                        content: "/admin/addpage"
                    });

                    parent.layui.table.reload("demo");
                    break;

            }

        });



        //监听行工具事件
        table.on('tool(test)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            var data = obj.data //获得当前行数据
                , layEvent = obj.event; //获得 lay-event 对应的值
            if (layEvent === 'update') {
                var id = data.id;
                layer.open({
                    title: "修改用户",
                    type: 2,
                    area:['600px', '300px'],
                    fixed: false, //不固定
                    maxmin: true,
                    content: "/admin/updatepage/"+id
                });


            } else if (layEvent === 'delete') {
                var id = data.id;


                $.ajax({
                    url: "/admin/userdelete",
                    type: "get",
                    dataType: "json",
                    data: {id: id},
                    success: function (data) {
                        parent.layui.table.reload("demo");
                    }
                });
            }
        });


    });


</script>
</body>
</html>

