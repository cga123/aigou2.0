<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/static/common/common.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" type="image/x-icon" href="${ctx }/static/img/title-icon.jpg"/>
<title>修改用户</title>

	<script src="/static/plugs/layui/layui.js"></script>
	<script src="/static/plugs/jquery.js"></script>
<script type="text/javascript">

	$(function(){
		$('.bt_close').on('click', function(){
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭 
			return false;
		})
	})

	layui.use(['form', 'upload', 'jquery'], function () {
		var form = layui.form;
		var upload = layui.upload;
		var $ = layui.jquery;

		$(".bt_save").click(function () {
			$.ajax({
				url: "/admin/updateuser?"+$('form').serialize(),
				type: "get",
				dataType: "json",
				success: function (data) {
					if(data > 0){
						parent.layer.msg('修改成功', {icon: 1});
						parent.layui.table.reload("demo");

						var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
						parent.layer.close(index); //再执行关闭
					}else{
						parent.layer.msg('修改失败', {icon: 1});
					}

				}
			})
		})

	})
</script>
</head>
<body>
	<div class="hp-context-page">
		<form action="#" class="hp-form">
			<input type="hidden" name="id" value="${user.id}">
			<div class="hp-form-item">
				<label class="hp-form-label">用户名称</label>
				<div class="hp-input-block">
					<input class="hp-input" type="text" name="username" value="${user.username }">
				</div>
			</div>
			<div class="hp-form-item">
				<label class="hp-form-label">用户密码</label>
				<div class="hp-input-block">
					<input class="hp-input" type="text" name="password" value="${user.password}">
				</div>	
			</div>
			<div class="hp-form-item" style="color: red">

			</div>
			<div class="hp-form-item">
				<button class="bt_save" lay-submit lay-filter="formDemo">保存</button>
				<button class="bt_close">关闭</button>
			</div>
		</form>
	</div>
</body>
</html>