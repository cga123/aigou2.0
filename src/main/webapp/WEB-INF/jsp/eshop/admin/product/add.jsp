<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2021/4/28
  Time: 14:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/static/plugs/layui/css/layui.css">
</head>
<body style="margin: 10px">

<form class="layui-form" action="">

    <div class="layui-form-item">
        <label class="layui-form-label">商品名称</label>
        <div class="layui-input-block">
            <input class="layui-input" type="text" name="productName">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">商品图片</label>
        <div class="layui-input-block" id="productImage">
            <button type="button" class="layui-btn" id="test1">上传图片</button>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">商品价格</label>
        <div class="layui-input-block">
            <input class="layui-input" type="text" name="price">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">商品详情图</label>
        <div class="layui-input-block" id="#">
            <button type="button" class="layui-btn" id="test2">上传图片</button>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">所属分类</label>
        <div class="layui-input-block">
            <select class="layui-input" name="productType">
                <c:forEach items="${productTypes}" var="productType">
                    <option value="${productType.id }">${productType.productTypeName }</option>
                </c:forEach>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">商品品牌</label>
        <div class="layui-input-block">
            <select class="layui-input" name="brandName">
                <c:forEach items="${brands}" var="brand">
                    <option value="${brand.id }">${brand.brandName }</option>
                </c:forEach>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script src="/static/plugs/layui/layui.js"></script>
<script>
    layui.use(['form', 'upload', 'jquery'], function () {
        var form = layui.form;
        var upload = layui.upload;
        var $ = layui.jquery;

        form.on('submit(formDemo)', function (data) {
            console.log(data.field);

            //发送ajax请求更新数据
            $.post('/pp/add', data.field, function (d) {
                console.log(d);
                parent.layer.msg('添加成功', {icon: 1});
                parent.layui.table.reload("demo");

                //当你在iframe页面关闭自身时
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                parent.layer.close(index); //再执行关闭
            })
            return false;
        });

        var uploadInst = upload.render({
            elem: '#test1'
            ,url: '/brand/upload/' //改成您自己的上传接口
            ,before: function(obj){
                obj.preview(function(index, file, result){
                    /* $('#demo1').attr('src', result); //图片链接（base64）*/
                    $('#img').prepend("<img class='layui-upload-img' width='100px' src='"+result+"'>")
                });
            }
            ,done: function(res){
                $('#url').val(res.data);
            }
        });

        var uploadInst = upload.render({
            elem: '#test2'
            ,url: '/brand/upload/' //改成您自己的上传接口
            ,before: function(obj){
                obj.preview(function(index, file, result){
                    /* $('#demo1').attr('src', result); //图片链接（base64）*/
                    $('#img').prepend("<img class='layui-upload-img' width='100px' src='"+result+"'>")
                });
            }
            ,done: function(res){
                $('#url').val(res.data);
            }
        });
    });
</script>
</body>
</html>
