<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script src="/static/plugs/jquery.js"></script>
</head>
<body>

<script type="text/html" id="toolDemo">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="getadds">添加</button>
    </div>
</script>


<div class="layui-form-item">
    <label class="layui-form-label">所属分类</label>
    <div class="layui-input-block">
        <select class="layui-input" name="productTypeName" id="demoReload">
            <option value="da">--无--</option>
            <c:forEach items="${brandLQS}" var="brandLQS">
                <option value="${brandLQS.id }">${brandLQS.productTypeName }</option>
            </c:forEach>
        </select>
    </div>
</div>
<button class="layui-btn" data-type="reload">搜索</button>





<table id="demo" lay-filter="test"></table>

<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="update">修改</a>
    <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del">删除</a>
</script>

<script>
    layui.use(['table'], function () {
        var table = layui.table;

        //头工具栏事件
        table.on('toolbar(test)', function(obj){
            switch(obj.event) {
                //增加用户
                case 'getadds':
                    layer.open({
                        title: "添加用户",
                        type: 2,
                        area: ['700px', '450px'],
                        fixed: false, //不固定
                        maxmin: true,
                        content: "/pp/addpage"
                    });
                    parent.layui.table.reload("demo");
                    break;
            }

        });


        //执行渲染
        table.render({
            elem: '#demo'
            ,url: '/pp/page/' //数据接口
            ,title: '品牌图'
            ,toolbar: '#toolDemo' //开启头部工具栏，并为其绑定左侧模板
            ,page: true //开启分页
            ,limit: 3
            ,limits: [2,3,5]
            ,cols: [[ //表头
                {type: 'checkbox', fixed: 'left'}
                ,{field: 'id', title: 'ID'}
                ,{field: 'productName', title: '商品名称'}
                ,{field: 'productImage', title: '商品图片',templet:function (d) {
                        return '<img src="/show/' + d.productImage + '"></img>'
                    }}
                ,{field: 'price', title: '商品价格'}
                ,{field: 'productTypeName', title: '所属分类'}
                ,{field: 'brandName', title: '商品品牌'}
                ,{field: 'createTime', title: '创建时间'}
                ,{fixed: 'right', align:'center', toolbar: '#barDemo', width: 200}
            ]]
            ,id:'testReload'
            ,request:{
                pageName: 'page'
                ,limitName: 'limit'
            }
        });



        var $ = layui.$, active = {
            reload: function(){
               // var demoReload = $('#demoReload');
               var demoReload =  $("#demoReload option:selected").text();
                console.log("获取:" + demoReload);
                //执行重载
                table.reload('testReload', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    ,where: {
                        key: {
                            'productTypeName': demoReload
                        }
                    }
                }, 'data');
            }
        };

        $('.layui-btn').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });


        table.on('tool(test)', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            var data = obj.data //获得当前行数据
                ,layEvent = obj.event; //获得 lay-event 对应的值
            if(layEvent === 'update'){
                //修改
                layer.open({
                    title: "修改轮播图",
                    type: 2,
                    area: ['700px', '450px'],
                    fixed: false, //不固定
                    maxmin: true,
                    content: '/pp/update/'+data.id
                });

            } else if(layEvent === 'del'){
                //删除
                layer.confirm('真的删除吗?',function (index) {
                    $.ajax({
                        url: '/pp/delete/'+data.id,
                        type: 'get',
                        success:function(data){
                          table.reload('demo');
                        }
                    });
                    layer.close(index);
                });
            }



        });

    });



</script>
</body>
</html>
