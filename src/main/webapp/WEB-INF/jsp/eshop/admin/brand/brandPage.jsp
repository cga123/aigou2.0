
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/plugs/layui/css/layui.css"  media="all">
    <link rel="stylesheet" type="text/css" href="/static/iconfont/iconfont.css">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>

<table class="layui-hide"  id="demo" lay-filter="demo"></table>
<script src="/static/plugs/jquery.js"></script>
<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container">

        <button class="layui-btn layui-btn-sm" lay-event="getCheckLength">添加品牌维护</button>
        <button class="layui-btn layui-btn-sm" lay-event="isAll">验证是否全选</button>
    </div>
</script>

<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>


<script src="/static/plugs/layui/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述 JS 路径需要改成你本地的 -->

<script>
    layui.use('table', function(){
        var table = layui.table;

        table.render({
            elem: '#demo'
            ,url:'${ctx}/brand/findByAll'
            ,toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
            ,page: true //是否显示分页
            ,limit: 10 //每页默认显示的数量
            ,defaultToolbar: ['filter', 'exports', 'print', { //自定义头部工具栏右侧图标。如无需自定义，去除该参数即可
                title: '提示'
                ,layEvent: 'LAYTABLE_TIPS'
                ,icon: 'layui-icon-tips'
            }]
            ,title: '用户数据表'
            ,cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'id', title:'ID', width:'25%', fixed: 'left', unresize: true, sort: true}
                ,{field:'brandName', title:'商品品牌名称', width:'15%'}
                ,{field:'brandType', title:'所属分类', width:'15%'}
                ,{field:'brandImg', title:'品牌logo', width:'30%',templet: function(d){
                    // <img src="/show/'+ d.url +'">
                        return '<img src="/show/'+d.brandImg+'"></img>'}}
                ,{fixed: 'right', title:'操作', toolbar: '#barDemo', width:'15%'}
            ]]
            ,page: true,
            id : 'demo'
        });

        //头工具栏事件
        table.on('toolbar(demo)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){

                case 'getCheckLength':
                    layer.open({
                        title: "修改商品分类维护",
                        type: 2,
                        area: ['700px', '350px'],
                        fixed: false, //不固定
                        maxmin: true,
                        content: '/brand/addPage/',
                    });

                    break;
                case 'isAll':
                    layer.msg(checkStatus.isAll ? '全选': '未全选');
                    break;

                //自定义头工具栏右侧图标 - 提示
                case 'LAYTABLE_TIPS':
                    layer.alert('这是工具栏右侧自定义的一个图标按钮');
                    break;
            };
        });

        /*******************************************************************************************/
        /*修改方法*/
        //监听工具条
        table.on('tool(demo)', function(obj) {
            var data = obj.data;
            if (obj.event === 'edit') {

                layer.open({
                    title: "修改商品分类维护",
                    type: 2,
                    area: ['600px', '450px'],
                    fixed: false, //不固定
                    maxmin: true,
                    content: '/brand/updatePage/'+data.id,
                });






                /*删除方法*/
            } else if (obj.event === 'del') {

                layer.confirm('真的删除行么', function(index) {

                    $.ajax({
                        url : '${ctx}/brand/del/'+ data.id,
                        type : 'get',
                        success : function(data) {
                            console.log(data)
                            if (data.code === 0) {
                                <!--成功的业务逻辑-->
                                table.reload('demo');
                            } else {
                                <!--失败处理的业务逻辑-->
                            }
                        }
                    });
                    layer.close(index);
                });
            }
        });
        /*******************************************************************************************/

    });
</script>

</body>
</html>
