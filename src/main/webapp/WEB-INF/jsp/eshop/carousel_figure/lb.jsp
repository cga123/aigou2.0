<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<table id="demo" lay-filter="demo"></table>
<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="getadds">添加</button>
    </div>
</script>
<script src="/static/plugs/jquery.js"></script>

<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="update">修改</a>
    <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del">删除</a>
</script>
<script src="/static/plugs/layui/layui.js" charset="utf-8"></script>
<script src="/static/plugs/jquery.js"></script>
<script>
    layui.use(['table'], function () {
        var table = layui.table;
        //执行渲染
        table.render({
            elem: '#demo'
            ,url: '/Lb/page/' //数据接口
            ,toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
            ,title: '轮播图'
            ,page: true //开启分页
            ,cols: [[ //表头
                {type: 'checkbox', fixed: 'left'}
                ,{field: 'id', title: 'ID'}
                ,{field: 'url', title: '轮播图',templet: function(d){
                        return '<img src="/show/'+ d.url +'"></img>'
                    }}
                ,{field: 'sequenceNum', title: '顺序'}
                ,{fixed: 'right', align:'center', toolbar: '#barDemo', width: 200}
            ]]
            ,id : 'demo'
        });

        //头工具栏事件
        table.on('toolbar(demo)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
                case 'getadds':
                    layer.open({
                        title: "添加轮播图",
                        type: 2,
                        area: ['600px', '300px'],
                        fixed: false, //不固定
                        maxmin: true,
                        content: '/Lb/addPage/',
                    });

            };
        });

        //===============


        table.on('tool(demo)', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            var data = obj.data //获得当前行数据
                ,layEvent = obj.event; //获得 lay-event 对应的值
            if(layEvent === 'update'){
                //修改
                layer.open({
                    title: "修改轮播图",
                    type: 2,
                    area: ['600px', '330px'],
                    fixed: false, //不固定
                    maxmin: true,
                    content: '/Lb/update/'+data.id
                });

            } else if(layEvent === 'del'){
                //删除
                layer.confirm('真的删除行么', function(index) {

                    $.ajax({
                        url : '${ctx}/Lb/del/'+ data.id,
                        type : 'get',
                        success : function(data) {
                            console.log(data)
                            if (data.code == 0) {

                                table.reload('demo');
                            } else {
                            }
                        }
                    });
                    layer.close(index);
                });
            }
        });

    });

</script>
</body>
</html>
