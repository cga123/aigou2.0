<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Title</title>
	<link rel="stylesheet" href="/static/plugs/layui/css/layui.css">
</head>
<body style="margin: 10px">

<form class="layui-form" action="">
	<input name="url" id="url" type="hidden" value="${carouselFigure.url}">
	<div class="layui-form-item">
		<label class="layui-form-label">上传图片</label>
		<div class="layui-input-block" id="img">
			<button type="button" class="layui-btn" id="test1">上传图片</button>
		</div>
	</div>

	<div class="layui-form-item">
		<label class="layui-form-label">顺序</label>
		<div class="layui-input-block">
			<input class="layui-input" type="text" name="sequenceNum" >
		</div>
	</div>

	<div class="layui-form-item">
		<div class="layui-input-block">
			<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
			<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		</div>
	</div>
</form>

<script src="/static/plugs/layui/layui.js"></script>
<script>
	layui.use(['form', 'upload', 'jquery'], function () {
		var form = layui.form;
		var upload = layui.upload;
		var $ = layui.jquery;

		form.on('submit(formDemo)', function (data) {
			console.log(data.field);

			//发送ajax请求更新数据
			$.post('/Lb/add', data.field, function (d) {
				console.log(d);
				parent.layer.msg('添加成功', {icon: 1});
				parent.layui.table.reload("demo");

				//当你在iframe页面关闭自身时
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index); //再执行关闭
			})
			return false;
		});

		var uploadInst = upload.render({
			elem: '#test1'
			,url: '/Lb/upload/' //改成您自己的上传接口
			,before: function(obj){
				obj.preview(function(index, file, result){
					//$('#demo1').attr('src', result); //图片链接（base64）
					$('#img').prepend("<img class='layui-upload-img' width='100px' src='"+result+"'>")
				});
			}
			,done: function(res){
				$('#url').val(res.data);
			}
		});

	});
</script>
</body>
</html>
